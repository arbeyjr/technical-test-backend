package com.technicaltest.app.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technicaltest.app.model.Person;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonService {


    private List<Person> persons;

    @PostConstruct
    public void init() {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Person>> typeReference = new TypeReference<List<Person>>() {};
        InputStream inputStream = TypeReference.class.getResourceAsStream("/data/persona.json");
        System.out.println("valor de inputstream: " + inputStream);
        try {
            persons = mapper.readValue(inputStream, typeReference);
            System.out.println("valor personas: " + persons);

        } catch (IOException e) {
            throw new RuntimeException("Fallo al leer el archivo json", e);
        }
    }


    public List<Person> getAllPersons() {
        return persons;
    }


    public List<Person> getPersonByFilter (String documentType, String documentNumber) {
        return persons.stream()
                .filter(person -> (documentType == null || person.getdocumentType().equals(documentType)) &&
                        (documentNumber == null || person.getdocumentNumber().equalsIgnoreCase(documentNumber)))
                .collect(Collectors.toList());
    }
}
