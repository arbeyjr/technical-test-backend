package com.technicaltest.app.controller;

import com.technicaltest.app.model.ApiResponse;
import com.technicaltest.app.model.Person;
import com.technicaltest.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/person")
public class PersonController {


    @Autowired
    private PersonService personService;


    @GetMapping("/all-persons")
    public ResponseEntity<ApiResponse<List<Person>>> getAllPersonas() {
        List<Person> personas = personService.getAllPersons();
        ApiResponse<List<Person>> response = new ApiResponse<>(HttpStatus.OK.value(), "Successfully", personas);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/person-filter")
    public ResponseEntity<ApiResponse<List<Person>>> getPersonByFilters(
            @RequestParam(required = true) String documentType,
            @RequestParam(required = true) String documentNumber) {
        List<Person> personas = personService.getPersonByFilter(documentType, documentNumber);
        if (personas.isEmpty()) {
            ApiResponse<List<Person>> response = new ApiResponse<>(HttpStatus.NOT_FOUND.value(), "No se encontró persona con los filtros suministrados.", null);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        ApiResponse<List<Person>> response = new ApiResponse<>(HttpStatus.OK.value(), "Successfully", personas);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
