package com.technicaltest.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

    private Long id;
    private String documentType;
    private String documentNumber;
    private String firstName;
    private String middleName;
    private String firstLastName;
    private String middleLastName;
    private String phone;
    private String address;
    private String cityResidence;

    public Person() {}

    public Person(
            @JsonProperty("id") Long id,
            @JsonProperty("documentType") String documentType,
            @JsonProperty("documentNumber") String documentNumber,
            @JsonProperty("firstName") String firstName,
            @JsonProperty("middleName") String middleName,
            @JsonProperty("firstLastName") String firstLastName,
            @JsonProperty("middleLastName") String middleLastName,
            @JsonProperty("phone") String phone,
            @JsonProperty("address") String address,
            @JsonProperty("cityResidence") String cityResidence
    ) {
        this.id = id;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.firstName = firstName;
        this.middleName = middleName;
        this.firstLastName = firstLastName;
        this.middleLastName = middleLastName;
        this.phone = phone;
        this.address = address;
        this.cityResidence = cityResidence;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getdocumentType() {
        return documentType;
    }

    public void setdocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getdocumentNumber() {
        return documentNumber;
    }

    public void setdocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getfirstName() {
        return firstName;
    }

    public void setfirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getmiddleName() {
        return middleName;
    }

    public void setmiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getfirstLastName() {
        return firstLastName;
    }

    public void setfirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public String getmiddleLastName() {
        return middleLastName;
    }

    public void setmiddleLastName(String middleLastName) {
        this.middleLastName = middleLastName;
    }

    public String getphone() {
        return phone;
    }

    public void setphone(String phone) {
        this.phone = phone;
    }

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    public String getcityResidence() {
        return cityResidence;
    }

    public void setcityResidence(String cityResidence) {
        this.cityResidence = cityResidence;
    }
}
